# Informix DB Drivers Installation

Instructions on how to install Informix DB drivers on Ubuntu server and connecting with PHP


Installing Informix DB drivers on a Linux machine can be a bit tedious. Following are instructions for installing the Informix DB drivers and then making a seccessful connection to Informix remote instance:

Steps 1: Install PHP & php-dev7.0 (You could already have these installed so can skip this)

"sudo apt-get install php libapache2-mod-php php-mcrypt php-mysql php-dev7.0"

Step 2:Install Informix ClientSDK

First make yourself the Super User. Simply run "sudo su"

Then create a new directory inside opt. Command: "mkdir /opt/informix"
Go into the dir: "cd /opt/informix/"
Download Informix CSDK: "wget https://iwm.dhe.ibm.com/sdfdl/2v2/regs2/mstadm/informix/Xa.2/Xb.YBTN_DlRQi0dD-LiGbnk3OJNbWlkWbAyJ0nNSjhU-k4/Xc.clientsdk.4.10.FC9DE.LINUX.tar/Xd./Xf.LPr.D1vk/Xg.9977259/Xi.ifxdl/XY.regsrvs/XZ.nQkPOrMpADBbq24gj4MkgfDR9kg/clientsdk.4.10.FC9DE.LINUX.tar"
** The wget URL might not work as it changes frequently, so just visit IBM website and download the version for your server type.

Step 3: Extract Zip and Install driver

extract archive: "tar -xvf client*.tar"
Install drivers: "./installclientsdk"
*just accept all defaults and let the installation complete...

Step 4: Install PDO_INFORMIX driver
Navigate to your home directory "cd /home/ubuntu"  --- ubuntu is the username, please replace with yours respectively
Download this: wget https://pecl.php.net/get/PDO_INFORMIX-1.3.2.tgz
Extract archive: tar -xvf PDO_INFORMIX-1.3.2.tgz
Go into directory: cd PDO_INFORMIX-1.3.2/
run this command: phpize

using your favority text editor, open the configure file: "sudo nano configure"
search for "ext/pdo/php_pdo_driver.h" and add this:

  elif test -f $prefix/include/php/20151012/ext/pdo/php_pdo_driver.h; then
    pdo_inc_path=$prefix/include/php/20151012/ext
	
**This is for users who are running PHP7.0 If you are running any previour or successor version of PHP, the above lines will change slightly. You can edit the lines above respectively or let me know I will help....

After editing, save the file and run this command: "./configure --with-pdo-informix=/opt/IBM/informix --with-php-config=/usr/bin/php-config7.0"
Then run "make"
And then "make install"
Then: echo "extension=pdo_informix.so" > /etc/php/7.0/mods-available/pdo_informix.ini
Then: ln -s /etc/php/7.0/mods-available/pdo_informix.ini /etc/php/7.0/fpm/conf.d/20-pdo_informix.ini (if it errors, ignore it)
Then: ln -s /etc/php/7.0/mods-available/pdo_informix.ini /etc/php/7.0/apache2/conf.d/20-pdo_informix.ini"

Now Edit /etc/apache2/envvars: "sudo nano /etc/apache2/envvars"
Simply add the following anywhere in this file, prefer it to be added to the top or towards the end.

#Adding environment variables
export INFORMIXDIR=/opt/IBM/informix/
export LD_LIBRARY_PATH=/opt/IBM/informix/lib:/opt/IBM/informix/lib/esql:/opt/IBM/informix/lib/cli:/opt/IBM/informix/lib/c++:/opt/IBM/informix/lib/client:/opt/IBM/informix/lib/dmi



That's all! Create a info.php and check for env var in this file. You should see the exports in the environment variables section and also informix support avaailable. 

Now, simply run the test connection as follows:

<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
$conexion = new PDO("informix:host=34.218.89.169;service=1526;database=isent98;server=ol_omsdb3;protocol=onsoctcp;EnableScrollableCursors=1;client_locale=en_us.8859-1;db_locale=en_us.8859-1","ReadOnly","nowriting");
print "Connection Established!\n\n";
?>

If the setup was successful above, you will see "Connection Established!"

Thanks! Let me know if you face any issues and I can assist. You can contact me at jack@iivo.io or jack@iivo.co.uk Phone:+91-8826622177

----
Jack
